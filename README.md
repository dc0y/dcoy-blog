### My blog's Repo

Build badge, why not: [![Build Status](https://travis-ci.org/obsidianspork/dcoy.svg?branch=master)](https://travis-ci.org/obsidianspork/my-blog)

This is the repo to store my blog: [https://david-coy.com](https://david-coy.com).

The blog itself is hosted in an AWS S3 bucket, served by CloudFront. I use [TravisCI](https://travis-ci.org) to perform the "build" process using [Hugo](https://gohugo.io).

More to come...
